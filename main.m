% some perf tweak
ignore_function_time_stamp='all';
PRINTF_OFFSET = 70;

if ~exist('dataSize', 'var')
	dataSize = 10000;
	dataOffset = 0;
end
patchSize = 6;
numCentroids = 800;
numPatches = 1600000;
stride = 1;	% make sure this value is < patchSize
numHiddenLayers = 1;
hiddenLayerSize = 2800;
numLabels = 10;
if ~exist('iter', 'var')
	iter = 50;
end
if ~exist('lambda', 'var')
	lambda = 0;
end

if ~exist('SKIP_TO_CLASSIFICATION', 'var') 
	offset = fprintf (stderr, 'Loading training data (dataSize=%d, dataOffset=%d)... ', dataSize, dataOffset);
	[ trainX, trainY ] = loadTrainingData(dataSize, dataOffset);
	fprintf (stderr, '%*s\n', PRINTF_OFFSET-offset, 'Done!!');

	X = single(convert2Gray(trainX));	% size(X) = [dataSize 1024]
	clear trainX;

	%----------------------------------------------------------------------
	%---PREPROCESSING STAGE---%
	%----------------------------------------------------------------------


	if ~exist('SKIP_PREPROCESSING', 'var')
		fprintf (stderr, '\nPREPROCESSING STAGE:\n\n');

		offset = fprintf (stderr, 'Extracting random patches (numPatches=%d)... ', numPatches);
		patches = extractRandomPatches(X, patchSize, numPatches);	% size(patches) = [numPatches patchSize*patchSize]
		fprintf (stderr, '%*s\n', PRINTF_OFFSET-offset, 'Done!!');

		offset = fprintf (stderr, 'Normalizing and applying ZCA transform... ');
		[ whitePatches, patchMean, covMat ] = zcaWhitening(normalize(patches, 2));
		fprintf (stderr, '%*s\n', PRINTF_OFFSET-offset, 'Done!!');
		clear patches;

		% Exit if libvlfeat not installed
		if ~exist('vl_kmeans', 'file')
			fprintf (stderr, stderr, 'Error: libvlfeat not installed!\nInstall with (Debian or Ubuntu):\n sudo apt-get install octave-vlfeat\n');
			return;
		end;

		offset = fprintf (stderr, 'Clustering patches (numCentroids=%d)... ', numCentroids);
		% Cluster using k-means (uses Elkan' triangle inequality based method)
		centroids = vl_kmeans(whitePatches', numCentroids, 'algorithm', 'elkan')';	% size(centroids) = [ numCentroids patcheSize*patchSize ]
		fprintf (stderr, '%*s\n', PRINTF_OFFSET-offset, 'Done!!');
		clear whitePatches;

		% DEMO
		if exist('DEMO_FLAG', 'var')
			figure;
			displayData(centroids(1:50,:));
		end
	end

	%----------------------------------------------------------------------
	%---END OF PREPROCESSING STAGE---%
	%----------------------------------------------------------------------

	%----------------------------------------------------------------------
	%---FEATURE EXTRACTION STAGE---%
	%----------------------------------------------------------------------

	fprintf (stderr, '\nFEATURE EXTRACTION STAGE:\n\n');

	Xfeatures = extractFeatures(X, centroids, stride, patchSize, patchMean, covMat);
	clear X;
	[ Xfeatures, XfeaturesMean, XfeaturesSig ] = normalize(Xfeatures, 1);

	%----------------------------------------------------------------------
	%---END OF FEATURE EXTRACTION STAGE---%
	%----------------------------------------------------------------------
end;

%return;
%----------------------------------------------------------------------
%---CLASSIFICATION STAGE---%
%----------------------------------------------------------------------

fprintf (stderr, '\nCLASSIFICATION STAGE:\n\n');

inputLayerSize = size(Xfeatures,2);
%hiddenLayerSize = inputLayerSize./4;

% Initialize Neural Network Parameters 
offset = fprintf (stderr, 'Initializing weights... ');

if ~exist('nn_params', 'var') || size(nn_params,1) != (inputLayerSize+1)*hiddenLayerSize+numLabels*(hiddenLayerSize+1)
	initial_Theta1 = randInitializeWeights(inputLayerSize, hiddenLayerSize);
	initial_Theta2 = randInitializeWeights(hiddenLayerSize, numLabels);
	% Unroll parameters
	initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];
	clear initial_Theta1;
	clear initial_Theta2;
else
	offset += fprintf (stderr, '(using existing ones)');
	initial_nn_params = nn_params;
end;

fprintf (stderr, '%*s\n', PRINTF_OFFSET-offset, 'Done!!');

% =======================================

fprintf(stderr,'Training Neural Network... \n')

options = optimset('MaxIter', iter);
% Create "short hand" for the cost function to be minimized
costFunction = @(p) nnCostFunction(p, ...
                                   inputLayerSize, ...
                                   hiddenLayerSize, ...
                                   numLabels, Xfeatures, trainY, lambda);
[nn_params, cost] = fmincg(costFunction, initial_nn_params, options);
clear initial_nn_params;

% Obtain Theta1 and Theta2 back from nn_params
Theta1 = reshape(nn_params(1:hiddenLayerSize * (inputLayerSize + 1)), ...
                 hiddenLayerSize, (inputLayerSize + 1));

Theta2 = reshape(nn_params((1 + (hiddenLayerSize * (inputLayerSize + 1))):end), ...
                 numLabels, (hiddenLayerSize + 1));


%----------------------------------------------------------------------
%---END OF CLASSIFICATION STAGE---%
%----------------------------------------------------------------------

%----------------------------------------------------------------------
%---PREDICTION AND RESULTS
%----------------------------------------------------------------------

% predict labels using calculated parameters
pred = predict(Theta1, Theta2, Xfeatures);
accuracy = mean(double(pred == trainY)) * 100;
fprintf (stderr, '\nTraining Set Accuracy: %f\n', accuracy);

% save current run data
record_stats([ dataOffset dataSize, numPatches, numCentroids, size(Xfeatures,2), stride, hiddenLayerSize, lambda, iter, cost(end), accuracy ], 'training_stats');

clear pred;

if ~exist('SKIP_CROSS_VALIDATE', 'var')
	if ~exist('SKIP_CROSS_FEATURE_EXTRACTION')
		if ~exist('crossDataSize')	crossDataSize = 3000; crossDataOffset = 40000; end;

		fprintf (stderr, '\nCROSS VALIDATION\n\n');
		offset = fprintf (stderr, 'Loading cross validation data (dataSize=%d)... ', crossDataSize);
		[ crossX, crossY ] = loadTestData(crossDataSize, crossDataOffset);
		fprintf (stderr, '%*s\n', PRINTF_OFFSET-offset, 'Done!!');

		Xc = single(convert2Gray(crossX));	% size(X) = [crossDataSize 1024]
		clear crossX;

		%----------------------------------------------------------------------
		%---FEATURE EXTRACTION STAGE---%
		%----------------------------------------------------------------------

		fprintf (stderr, '\nFEATURE EXTRACTION STAGE:\n\n');
		
		Xcfeatures = extractFeatures(Xc, centroids, stride, patchSize, patchMean, covMat);
		clear Xc;
		Xcfeatures = bsxfun(@rdivide, bsxfun(@minus, Xcfeatures, XfeaturesMean), XfeaturesSig);
		Y = bsxfun(@eq, crossY(:), 1:numLabels);
	end;

	%----------------------------------------------------------------------
	%---END OF FEATURE EXTRACTION STAGE---%
	%----------------------------------------------------------------------

	[predt, h] = predict(Theta1, Theta2, Xcfeatures);
	J = 1./crossDataSize.*(sum(sum((-Y.*log(h)-(1-Y).*log(1-h)), 2))); 
	fprintf (stderr, '\nCost: %f', J);
	accuracy = mean(double(predt == crossY)) * 100;
	fprintf (stderr, '\nCross Validation Set Accuracy: %f\n', accuracy);

	record_stats([ crossDataOffset, crossDataSize, numCentroids, size(Xcfeatures,2), stride, lambda, J, accuracy ], 'cross_validation_stats');

	clear predt;
	clear J;
end

%EOF
