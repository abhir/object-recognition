function heatmap(X, xlab, ylab, title_string)

colormap('hot');
imagesc(X);
ylabel(ylab);
xlabel(xlab);
title(title_string);
colorbar;

end;
