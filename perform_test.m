% WARNING: Execute main.m before executing this file
if ~exist('PRINTF_OFFSET', 'var'), PRINTF_OFFSET= 70; end
if ~exist('patchSize', 'var'), patchSize = 6; end
if ~exist('stride', 'var'), stride = 1; end
if ~exist('numCentroids', 'var'), numCentroids = size(centroids, 1); end

if ~exist('testDataSize', 'var')
	if ~exist('dataSize', 'var'), testDataSize = 2000; 
	else testDataSize = dataSize .* 0.2; end
end

fprintf (stderr, '\nTESTING STAGE\n\n');

if ~exist('SKIP_TEST_FEATURE_EXTRACTION')
	offset = fprintf (stderr, 'Loading test data (dataSize=%d)... ', testDataSize);
	[ testX, testY ] = loadTestData(testDataSize);
	fprintf (stderr, '%*s\n', PRINTF_OFFSET-offset, 'Done!!');

	Xt = single(convert2Gray(testX));	% size(X) = [testDataSize 1024]
	clear testX;

	%----------------------------------------------------------------------
	%---FEATURE EXTRACTION STAGE---%
	%----------------------------------------------------------------------

	fprintf (stderr, '\nFEATURE EXTRACTION STAGE:\n\n');

	Xtfeatures = extractFeatures(Xt, centroids, stride, patchSize, patchMean, covMat);
	Xtfeatures = bsxfun(@rdivide, bsxfun(@minus, Xtfeatures, XfeaturesMean), XfeaturesSig);

	%----------------------------------------------------------------------
	%---END OF FEATURE EXTRACTION STAGE---%
	%----------------------------------------------------------------------

end

predt = predict(Theta1, Theta2, Xtfeatures);
accuracy = mean(double(predt == testY)) * 100;
fprintf (stderr, '\nTest Set Accuracy: %f\n', accuracy);

record_stats([ testDataSize, numCentroids, size(Xtfeatures,2), stride, accuracy ], 'test_stats');

% load the label data
load 'cifar-10-batches-mat/batches.meta.mat';

fprintf (stderr, '\nGenerating heatmap...\n');
heatmap (table(predt, testY), 'Actual', 'Predicted', 'Confusion Matrix Heatmap');
fprintf (stderr, 'Predicted vs Actual heatmap\n');
fprintf (stderr, '\nClasses:\n');

for i=1:size(label_names,1)
	fprintf (stderr, '%2d %s\n', i, label_names{i});
end

fprintf(stderr, '\nProgram paused. Press enter to continue.\n');
pause;

fprintf (stderr, '\nRunning examples...\n');

%  Randomly permute examples
rp = randperm(size(Xt, 1));

for i = 1:size(Xt, 1)
    % Display 
    displayImageMap(Xt(rp(i), :));

    fprintf(stderr, '\nNeural Network Prediction: %s : %s (Actual)\n', label_names(predt(rp(i))){1}, label_names(testY(rp(i))){1});
    fprintf(stderr, 'Program paused. Press enter to continue.\n');
    pause;
end

clear testY;
clear Xt;
clear Xtfeatures;
clear predt;
clear rp;

%EOF
