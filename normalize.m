function [X_norm, mu, sig] = normalize(X, dim, epsilon)
% NORMALIZE Normalize the data (subtract by mean and divide by std. deviation).
%	[X_norm] = NORMALIZE(X) operates on each row of matrix X and subtracts
%	each element of it by mean of it and divides the result by square root
%	of its variance + 10. In other words,
%                          X(i, j) - mean(X(i,:))
%          X_norm(i, j) = ----------------------- ; for row i and column j.
%                           sqrt(var(X(i,:)+10)

% Sensible default for dim
if ~exist('dim', 'var') || isempty(dim)
	dim = 1;
end;

if ~exist('epsilon', 'var') || isempty(epsilon)
	epsilon = 10;
end;

mu = mean(X, dim);
sig = sqrt(var(X, 0, dim) + epsilon);
X_norm = bsxfun(@rdivide, bsxfun(@minus, X, mu), sig);

end;
