function [Xwhite, patchMean, covMat] = zcaWhitening(X, epsilon)
% ZCAWHITENING Perform a ZCA whitening transform on X
%	[Xwhite, patchMean, covMat] = ZCAWHITENING(X, epsilon) returns the ZCA transform of
%	X in Xwhite. epsilon is the ZCA regularization parameter.
%

% Sensible default for epsilon
if ~exist('epsilon', 'var') || isempty(epsilon)
	epsilon = 0.01;
end;

% Mean
patchMean = mean(X);
% Eigendecomposition of covariance matrix
[V,D] = eig(cov(X));
% Scaled covariance matrix
covMat = V * diag(sqrt(1./(diag(D) + epsilon))) * V';
% white patches
Xwhite = bsxfun(@minus, X, patchMean) * covMat;

end;
