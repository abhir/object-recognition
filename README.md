object-recognition
==================

Object Recognition System based on [CIFAR-10 dataset](http://www.cs.toronto.edu/~kriz/cifar.html).
Implemented in GNU Octave.

Requirements
------------
1. GNU Octave. Get from [here](https://www.gnu.org/software/octave/download.html) or for Ubuntu/Debian users install with: `$ sudo apt-get install octave`.
2. Octave `image` and `statistics` packages. Get from [octave-forge](http://octave.sourceforge.net/packages.php).
3. [vlfeat](http://www.vlfeat.org/install-octave.html) library.

How-to test
-----------

1. Launch octave:
`$ octave`
2. Train the model:
`> main`
3. Run test:
`> perform_test`
