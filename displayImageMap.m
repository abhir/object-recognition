function [h, display_array] = displayImageMap(X, example_width, example_height)
%DISPLAYIMAGEMAP Display 2D data in a nice grid
%   [h, display_array] = DISPLAYIMAGEMAP(X, example_width) displays 2D data
%   stored in X in a nice grid. It returns the figure handle h and the 
%   displayed array if requested.

% Set example_width automatically if not passed in
if ~exist('example_width', 'var') || isempty(example_width) 
	example_width = round(sqrt(size(X, 2)));
end

if nargin < 3
	example_height = example_width;
end

[m n] = size(X);
dim = size(X, 2)/(example_width*example_height);
assert (dim == 3 || dim == 1);

% Compute number of items to display
display_rows = floor(sqrt(m));
display_cols = ceil(m / display_rows);

% Between images padding
pad = 1;

% Setup blank display
display_array = -ones(pad + display_rows * (example_height + pad), ...
                       pad + display_cols * (example_width + pad), dim);

% Copy each example into a patch on the display array
curr_ex = 1;
for j = 1:display_rows
	for i = 1:display_cols
		if curr_ex > m, 
			break; 
		end
		curr_image = reshape(X(curr_ex, :), example_height, example_width, dim);
		trans_image = curr_image;
		for k = 1:size(curr_image,1), trans_image(:,k,:) = curr_image(k,:,:); end
		if dim == 1, trans_image = trans_image/max(abs(trans_image(:))); end
		display_array(pad + (j - 1) * (example_height + pad) + (1:example_height), ...
		              pad + (i - 1) * (example_width + pad) + (1:example_width), :) = ...
					  trans_image;
		curr_ex = curr_ex + 1;
	end
	if curr_ex > m, 
		break; 
	end
end

% Display Image
if dim == 1
	h = imshow(display_array, [-1 1]);
else
	h = imshow(display_array/255);
end

% Do not show axis
axis image off

end
