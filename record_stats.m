function record_stats(X, stats_file)
% RECORD_STATS Records the stats of the current run into file "stats_file"
%	RECORD_STATS(X, stats_file)
%	saves the input parameters in file "stats_file".

% set to default if not passed in
if ~exist('stats_file', 'var') || isempty(stats_file) 
	stats_file = 'stats';
end


fid = fopen(stats_file, "a");
if fid == -1
	fprintf(stderr, 'record_stats: Error: Unable to open file %s for writing.\n', stats_file)
	return
end

fprintf(fid, '\t%g\t', X);
fprintf(fid, '\n');
fclose(fid);

end
